/* eslint-disable linebreak-style */
import fs from "fs";
import express from "express";
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(express.static("public"));

let books = [];

function updateJsonFile() {
    fs.writeFileSync("booksdb.json", JSON.stringify(books), "utf-8");
}

function readJsonFile() {
    try {
        books = JSON.parse(fs.readFileSync("booksdb.json", "utf-8"));
    } catch(error) {
        console.log("error happened here!");
        console.log(error);
        books = [];
    }
}

readJsonFile();
// tehdään kirjalle oma mallipohja
class Book {
    constructor(id, name, author, read) {
        this.id = id,
        this.name = name,
        this.author = author,
        this.read = read;
    }
}

// kaikki kirjat
app.get("/books", (req, res) => {
    res.send(books);
    console.log(books);   
});

// tietty kirja id:n mukaan
app.get("/books/:id", (req, res) => {
    const bookID = books.find(book => book.id === req.params.id);
    res.send(bookID);
    console.log(bookID);
});

// lisätään kirja taulukkoon
app.post("/books", (req, res) => {
    const newBook = new Book(
        req.body.id,
        req.body.name,
        req.body.author,
        req.body.read
    );
    books.push(newBook);
    updateJsonFile();
    res.send(newBook);
    console.log(newBook);
    console.log("Book added successfully.");
});

// muokataan olevassa olevaa kirjaa
app.put("/books/:id", (req, res) => {
    const updateBook = {...req.body, id: req.params.id};
    books = books.map(book => book.id === req.params.id ? updateBook : book);
    updateJsonFile();
    console.log("Book edited added successfully.");
});

// poistetaan kirja id:n mukaan
app.delete("/books/:id", (req, res) => {
    books = books.filter(book => book.id !== req.params.id);
    updateJsonFile();
    console.log("Book deleted successfully.");
});


app.listen(3000);