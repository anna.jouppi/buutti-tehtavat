/* eslint-disable linebreak-style */
import fs from "fs";
import express from "express";
import * as bcrypt from "bcrypt";
import { v4 as uuidv4 } from 'uuid';
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import {} from 'dotenv/config';
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static("public"));

let books = [];
let authenticated_users = [];

function updateJsonFile() {
    fs.writeFileSync("booksdb.json", JSON.stringify(books), "utf-8");
}
function updateJsonFileUser() {
    fs.writeFileSync("usersdb.json", JSON.stringify(authenticated_users), "utf-8");
}

function readJsonFile() {
    try {
        books = JSON.parse(fs.readFileSync("booksdb.json", "utf-8"));
    } catch(error) {
        console.log("error happened here!");
        console.log(error);
        books = [];
    }
    try {
        authenticated_users = JSON.parse(fs.readFileSync("usersdb.json", "utf-8"));
    } catch(error) {
        console.log("error happened here!");
        console.log(error);
        authenticated_users = [];
    }
}

readJsonFile();
// tehdään kirjalle oma mallipohja
class Book {
    constructor(id, ownerid, name, author, read) {
        this.ownerid = ownerid,
        this.id = id,
        this.name = name,
        this.author = author,
        this.read = read;
    }
}


// middleware for token
function authToken(req, res, next) {
    const authHeader =  req.headers['authorization'];
    const authToken = authHeader && authHeader.split(' ')[1]; // returns header and split token
    if (authToken === null) return res.sendStatus(401); // if token has not been sent

    // verifying token with env file's hashed secret
    jwt.verify(authToken, process.env.TOKEN_SECRET, (err, user) => {
        // sending forbidden status, if user has a token but it is not valid anymore
        if(err) return res.sendStatus(403);
        req.user = user;
        next();
    });
}



// lisätään kirja taulukkoon
app.post("/books", (req, res) => {
    const newBook = new Book(
        req.body.ownerid,
        req.body.id,
        req.body.name,
        req.body.author,
        req.body.read
    );
    books.push(newBook);
    updateJsonFile();
    res.send(newBook);
    console.log(newBook);
    console.log("Book added successfully.");
});

// listing all users
app.get("/users", (req, res) => {
    res.send(authenticated_users);
});

// listing all books
app.get("/books", (req, res) => {
    res.send(books);
});

// all books according to ownerid
app.get("/users/books", authToken, (req, res) => {
    res.json(books.filter(book => book.ownerid === req.user.id));
});

// a book according to book's id
app.get("/books/:id", (req, res) => {
    const bookID = books.find(book => book.id === req.params.id);
    res.send(bookID);
    console.log(bookID);
});

// updating a book according to ownerid
app.put("/users/books/:id", authToken, (req, res) => {
    
    if(books.filter(book => book.ownerid === req.user.id)) {
        const updateBook = {...req.body, id: req.params.id};
        books = books.map(book => book.id === req.params.id ? updateBook : book);
        updateJsonFile();
        res.send("Book edited successfully.");
    } else { res.send("You are not authorized to modify this book."); }
});    

// updating a book
app.put("/books/:id", (req, res) => {
    const updateBook = {...req.body, id: req.params.id};
    books = books.map(book => book.id === req.params.id ? updateBook : book);
    updateJsonFile();
    res.send("Book edited successfully.");
});

// deleting a book according to book id
app.delete("/books/:id", (req, res) => {
    books = books.filter(book => book.id !== req.params.id);
    updateJsonFile();
    console.log("Book deleted successfully.");
});


// registering user, creating UUID and hashing the password
app.post("/register", async (req, res) => {
    try {
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);
        
        const randomid = uuidv4();

        const newuser = {
            id: randomid,
            username: req.body.username,
            password: hashedPassword
        };
        authenticated_users.push(newuser);
        updateJsonFileUser();
    }
    catch(error) {
        console.log(error);
    }
});

// authenticating login and comparing password hashes
app.post("/users/login", async (req, res) => {
    const user = authenticated_users.find(newuser => newuser.username === req.body.username);

    // must stringify first, because it is in json format
    const token = jwt.sign(user, process.env.TOKEN_SECRET);
    if(user === null) {
        return res.send("User not found!");
    }

    try {
        if(await bcrypt.compare(req.body.password, user.password)) {
            res.json({accessToken : token});
            res.send("Successful login.");
            
        } else {
            res.send("Password incorrect.");
        }
    }
    catch {
        res.status(500).send();
    }
    // token_secret created with crypto's randombytes
    
   
});

app.listen(3000);