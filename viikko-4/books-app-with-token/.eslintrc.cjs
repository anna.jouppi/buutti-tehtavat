module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "windows"
        ],
        "eqeqeq": ["error", "always"],
        "no-var": "error",
        "semi": [
            "error",
            "always"
        ],
        "prefer-const": "error",
        "no-lonely-if": "error",
        "no-useless-return": "error",
        "no-constant-condition": "off",
        "no-redeclare": "error"
    }
};

