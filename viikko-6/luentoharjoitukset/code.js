function pressme() {
    window.alert("Hello!");
}

function greeting() {
    window.onload = () => {
    console.log("Hello visitor!");
}}



function clicktimes() {
    let clicks = 0;
    const button = document.getElementById("btn");
     button.onclick = () => {
        clicks++
        window.alert(`You have clicked the button ${clicks} times!`);
    }
}
clicktimes();