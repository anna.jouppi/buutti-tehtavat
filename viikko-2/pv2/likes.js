function likes(likesArray) {
    switch(likesArray.length) {
    case 0:     // viittaa indeksien lukumäärään
        console.log("No one likes this.");
        break;
    case 1:
        console.log( `${likesArray[0]} likes this.`);
        break;
    case 2:
        console.log( `${likesArray[0]} and ${likesArray[1]} likes this.`);
        break;
    case 3:
        console.log( `${likesArray[0]}, ${likesArray[1]} and ${likesArray[2]} likes this.`);
        break;
    case 4:
        console.log( `${likesArray[0]}, ${likesArray[1]} and ${likesArray.length - 2} others likes this.`);
        break;
    }

}

likes([]); // "no one likes this"
likes(["John"]); // "John likes this"
likes(["Mary", "Alex"]); // "Mary and Alex like this"
likes(["John", "James", "Linda"]); // "John, James and Linda like this"
likes(["Alex", "Linda", "Mark", "Max"]); // must be "Alex, Linda and 2 others