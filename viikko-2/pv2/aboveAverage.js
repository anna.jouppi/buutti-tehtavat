// Create a function that takes in an array and returns a 
// new array of values that are above average.
// aboveAverage([1, 5, 9, 3]) 
// outputs an array that has values greater than 4.5


function aboveAverage(array) {
    const sum = array.reduce(
        (accumulator, currentnumber) => accumulator + currentnumber);
    console.log(sum);
    const averageNum = sum / array.length;
    const newArray = array.map( greater => greater * averageNum + 1) ;
    return console.log(newArray);

}

aboveAverage([1, 5, 9, 3]);