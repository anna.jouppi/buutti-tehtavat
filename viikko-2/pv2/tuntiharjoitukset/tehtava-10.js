class Shape {
    constructor (width, height) {
        this.width = width;
        this.height = height;
    }
    area() {
        return this.height * this.width;
    }
    circle() {
        return this.height * this.width;
    }
    circumference() {
        return this.circle * 3.14;
    }
}

class Circle extends Shape {
    constructor (diameter) {
        super(diameter, diameter);
    }

    getCircumference() {
        return Math.PI *this.width;
    }

    getArea() {
        return 2 * Math.PI * (this.width / 2) ** 2;
    }
}

class Rectangle extends Shape {
    getCircumference() {
        return this.height  *this.width;
    }
    getArea() {
        return 2 * Math.PI * (this.width / 2) ** 2;
    }
}

class Triangle extends Shape {

}