function FacebookPost (poster, text, likes, comments) {
    this.poster = poster;
    this.text = text;
    const today = new Date();
    this.date = today.getDate() + "." + (today.getMonth() + 1) + "." + today.getFullYear();
    this.likes = 0;
    this.comments = [];
    this.deleted = false;
}

FacebookPost.prototype.edit = function(newText) {
    this.text = newText;
};

FacebookPost.prototype.delete = function() {
    this.deleted = true;
};

FacebookPost.prototype.like = function() {
    this.likes++;
};

FacebookPost.prototype.comment = function(comment) {
    this.comment.push(comment);

};

class NotFacebookComment extends FacebookPost {
    print() {
    console.log(`${this.poster} commented at ${this.date}: ${this.text}`);
}
}

