// function calculator(operator, num1, num2) {
// if (operator === "+") return console.log(num1 + num2);
// else if (operator === "-") return console.log(num1 - num2);
// else if (operator === "/") return console.log(num1 / num2);
// else if (operator === "*") return console.log(num1 * num2);
// else if (operator !== "+" && operator !== "-" && operator !== "/" && operator !== "*") return console.log("Operator is invalid!");
// };

// calculator("6", 7, 3);

function calculator(operator, num1, num2) {
    switch (operator) {
    case "+":
        console.log(num1 + num2);
        break;
    case "-":
        console.log(num1 - num2);
        break;
    case "/":
        console.log(num1 / num2);
        break;
    case "*":
        console.log(num1 * num2);
        break;
    default:
        console.log("Invalid operator!");
        break;
    }
}

calculator("+", 7, 3);