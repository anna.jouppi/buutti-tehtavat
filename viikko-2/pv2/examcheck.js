// The first input array is the key to the correct answers to an exam, like ["a", "a", "b", "d"] . The second one contains a student's submitted answers.
// The two arrays are not empty and are the same length. Return the score for this array of answers, giving +4 for each correct answer, -1 for each
// incorrect answer, and +0 for each blank answer, represented as an empty string.
// If the score < 0, return 0

function checkExam(answers, student) {
    let score = 0;
    for (let i = 0; i < answers.length; i++) {

        if (student[i] === "") {
            score = score + 0;
        }
        else if (answers[i] !== student[i]) {
            score = score - 1;
        }
        else if (answers[i] === student[i] ) {
            score = score + 4;
        }
         
    } 

    return score;
}

console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "", ""]));


// Anna S:n ratkaisu:
// let score = 0;
//     for (let index = 0; index < arr1.length; index++) {
//         if (arr1[index] === arr2[index]) score += 4;
//         else if (arr2[index] !== "") score--;
//     }
//     if(score < 0) score = 0;
//     console.log(score);
// }