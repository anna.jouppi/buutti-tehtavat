// Define a number n that is larger than 0, for example n = 3
// Create a function that given parameter n finds the number of steps it takes to reach number 1 (one) using the following process
// If n is even, divide it by 2
// If n is odd, multiply it by 3 and add 1

let n = 3;
while (n !== 1) {

    if (n % 2 === 0 ) {
        n = n / 2;
    } else if (n % 2 !== 0 ) {
        n = n * 3 + 1;
       
    } console.log(n); 
    
}
