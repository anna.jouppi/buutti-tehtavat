// Create a function (or multiple functions) that generates username and password from given firstname and lastname.
// Username: B + last 2 numbers from current year + 2 first letters from both last name and first name in lower case
// Password: 1 random letter + first letter of first name in lowercase + last letter of last name in uppercase + random special character + last 2 numbers from
// current year
// Example: John Doe -> B20dojo, mjE(20
// generateCredentials("John", "Doe")



//username
function username(firstname, lastname) { 
    const date = new Date();  
    const year = date.getFullYear() -2000;
    const namesSplit = `${lastname.substring(0, 2)}${firstname.substring(0, 2)}`;
    const finalName = `B${year}${namesSplit.toLowerCase()}`;

    return console.log(finalName);

}
username("John", "Doe");

//password
function password(firstname, lastname) { 
    const date = new Date();  
    const year = date.getFullYear() -2000;
    const firstLetter = `${randomLetter(1)}`;
    const namesSplit = `${firstname.substring(0, 1)}`;
    const lower = namesSplit.toLowerCase();
    const lastSplit = lastname.slice(-1);
    const upper = lastSplit.toUpperCase();
    const randomChar =  `${randomCharacter(33, 47)}`;
    const finalPass = `${firstLetter}${lower}${upper}${randomChar}${year}`;

    return console.log(finalPass);
}

password("John", "Doe");

// random letter
function randomLetter(number) {
    let finalLetter = "";
    const alpha = "abcdefghijklmnopqrstuvwxyz";

    for (let i = 0; i < number; i++) {
        finalLetter += alpha.charAt(Math.floor(Math.random() * alpha.length));
    }
    return finalLetter;

}
//console.log(randomLetter(1));

//random character
function randomCharacter(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
     
    return String.fromCharCode(Math.floor(Math.random() * (max - min + 1) + min));

}


//console.log(randomCharacter(33, 47));