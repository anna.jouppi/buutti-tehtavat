// Create a program that generates 7 random numbers from 1
// to 40. Numbers must be unique.
// Return array of lottery numbers.

function randomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return console.log(Math.floor(Math.random() * (max - min + 1)) + min);

}
randomInt(1, 40);

const array = Array(7).fill().map(() => Math.floor(Math.random() * 40));
console.log(array);

const numbers = [4, 6, 2, 7, 8, 9, 5];

// while (numbers.length < 7) {
//     const lottoNumber = randomInt(1, 40);

//     if (!numbers.includes(lottoNumber)) {
//         numbers.push(lottoNumber);
//     }
// }

// console.log(numbers);

const list = [];
for (let i = 1; i<= 1000; i++) {
    list.push(randomInt(1,40));
}

const filteredList = list.filter((n, i) => !list.slice(i + 1).includes(n));
console.log(filteredList.slice());

let i = 0;
const timer = setInterval(() => {
    if(i === 7)
    clearInterval(timer);
    console.log(numbers[i]);
    i++;
}, 1000
);