const stack = [];
while(stack.length > 0) {
    //tällä voi korvata 3 riviä: const {x, y} = stack.pop();
    const element = stack.pop();
    const x = element.x;
    const y = element.y;
    canvas.setPixel(x, y).color = "red";
    
    if(canvas.getPixel(x - 1, y).color === "white") {
        stack.push({x: x - 1, y: y});
    }
    
    if(canvas.getPixel(x, y - 1).color === "white") {
        stack.push({x: x, y: y - 1});
    }
    
    if(canvas.getPixel(x + 1, y).color === "white") {
        stack.push({x: x + 1, y: y});
    }
    
    if(canvas.getPixel(x, y + 1).color === "white") {
        stack.push({x: x, y: y + 1});
    }
} 
