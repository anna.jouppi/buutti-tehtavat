// const min = process.argv[2];
// const max = process.argv[3];

// function randomNumber(min, max) {


// const randNumber = Math.floor(Math.random() * max);
// return console.log(randNumber);
// }

// randomNumber(min, max);

// generates a random number between min (inclusive) and max (inclusive)

function randomInt(min, max) {
    const range = max - min;
    return min + Math.random() * range;
}

console.log(randomInt(4, 9));
