// Create a function, takes an number n as parameter, and
// produces an array of numbers, n being equal to the
// length of the array, that follows this sequence:
// const arr = [1, 1, 2, 3, 5, 8, 13, 21, 34, …];

// function arrFunction(n) {
//     const arr = [];
    
//     for (let i = 1; i <= n; i++) {
//         const double = i * 6;
//         const minus = double - i;
//         arr.push(minus);
//     }
//     console.log(arr);
//     return arr;
   
// }
// arrFunction(10);

const fibo = [1, 1];

for(let i = 2; i < 20; i++) {
    fibo.push(fibo[i - 1] + fibo[i - 2]);
}

console.log(fibo);



