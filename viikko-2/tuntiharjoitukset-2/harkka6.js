const arr1 = [1, 2, 3, 4];
const arr2 = [3, 4, 5, 6];

function arrays() {
    const arr3 = arr1.concat(arr2);
    console.log(arr3);
    const arr4 = [...arr1, ...arr2];
    console.log(arr4);
    const arrDuplicates = arr4.filter((n, i) => !arr4.slice(i + 1).includes(n));
    console.log(arrDuplicates);
}
arrays();