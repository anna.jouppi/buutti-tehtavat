// Create a programs that reverses each word in a string.
// node .\reverseWords.js "this is a very long sentence" -> sihT si a yrev gnol ecnetnes

const inputStr = process.argv[2];

const arr = [];
const splitString = inputStr.split("");
const reversed = splitString.reverse();
const joined = reversed.join("");
arr.push(joined);

const finalString = `${arr}`;

console.log(finalString);