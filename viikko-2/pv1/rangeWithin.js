// Write a program that takes in any two numbers from the command line, start and end . The program creates and prints an array filled with numbers from
// start to end .
// Examples:
// node .\createRange.js 1 5 -> [1, 2, 3, 4, 5]
// node .\createRange.js -5 -1 -> [-5, -4, -3, -2, -1]
// node .\createRange.js 9 5 -> [9, 8, 7, 6, 5]
// Note the order of the values. When start is smaller than end , the order is ascending and when start is greater than end , order is descending.

const start = parseInt(process.argv[2]);
const end = parseInt(process.argv[3]);

const outputArr = [];
if (start < end) {
    for (let i = start; i <= end; i++) {
        outputArr.push(i);
    }}
else if (start > end) {
    for (let i = end; i <= start; i++) {
        outputArr.push(i);
        
    }
    outputArr.reverse();
}
    


console.log(outputArr);
