// const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
// Create a program that every time you run it, prints out an array
// with differently randomized order of the array above.
// Example:
// node .\randomizeArray.js -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10]

const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

const arrayRandom = function(arr) {
    let newPosition;
    let temporary;

    for (let i = arr.length - 1; i > 0; i--) {
        newPosition = Math.floor(Math.random() * (i + 1));
        temporary = arr[i];
        arr[i] = arr[newPosition];
        arr[newPosition] = temporary;
       
    }
    return arr;
};

const newArray = arrayRandom(array);

const newArray2 = arrayRandom(newArray);
console.log(newArray2);
