// Check if given string is a palindrome.
// Examples:
// node .\checkPalindrome.js saippuakivikauppias -> Yes, 'saippuakivikauppias' is a palindrome
// node .\checkPalindrome.js saippuakäpykauppias -> No, 'saippuakäpykauppias' is not a palindrome

const inputStr = process.argv[2];

// käännetään ensin stringi toisinpäin
function reverseString(str) {
    let reverseStr = "";
    for (let i = str.length - 1; i >= 0; i--) { //lasketaan taaksepäin
        reverseStr = reverseStr + str[i];
    }
    return reverseStr;

}

//tarkistetaan onko reverseString sama kuin annettu stringi

function checkPalindrome(str) {
    const reverseStr = reverseString(str);

    if(reverseStr === str) {
        console.log(`Yes, ${str} is a palindrome.`);
    }
    else {
        console.log(`No, ${str} is not a palindrome.`);
    }
}

checkPalindrome(inputStr);



