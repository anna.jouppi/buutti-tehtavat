// You have two arrays:
// const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"]; 
// const ordinals = ['st', 'nd', 'rd', 'th'];
// Create program that outputs competitors placements with following way: ['1st competitor was Julia', '2nd competitor was Mark', '3rd competitor
// was Spencer', '4th competitor was Ann', '5th competitor was John', '6th competitor was Joe']

const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"]; 

const ordinals = ['st', 'nd', 'rd', 'th'];


const newArray = [];
for (let i = 0; i < ordinals.length; i++) { // lisää lyhenteet uuteen arrayhin
    if (ordinals[i] === "st") newArray.push("1st");
    if (ordinals[i] === "nd") newArray.push("2nd");
    if (ordinals[i] === "rd") newArray.push("3rd");
    if (ordinals[i] === "th") newArray.push("4th", "5th", "6th");
    
}
console.log(newArray);

function placementString() { // uudesta taulukosta tehdään stringit molempien taulukoiden avulla
    for (let i = 0; i < newArray.length; i++) {
        const str = `${newArray[i]} competitor was ${competitors[i]}.`;
        console.log(str);
    }
}
placementString();




