// Create a program that turns any given word into charIndex version of the word
// Example:
// node .\charIndex.js "bead" -> 2514
// node .\charIndex.js "rose" -> 1815195

// const charIndex = { a : 1, b : 2, c : 3, d : 4, e : 5, ... , y : 25, z : 26 };

const inputStr = process.argv[2];

const alphabet = "abcdefghijklmnopqrstuvwxyz";

for (let i = 0; i < inputStr.length; i++) {
    const inputStrSplit = inputStr.split(""); // split palauttaa uuden arrayn
    // console.log(inputStrSplit);
    const charPos = alphabet.indexOf(inputStrSplit[i]) + 1; // jokaisen stringin indeksi erikseen läpi
    console.log(charPos);
}



console.log(inputStr);

