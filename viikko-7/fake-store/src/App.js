import './App.css';
import Form from './Form';


function App() {
  return (
    <div className="App">
      <header>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <span className="navbar-brand mb-0 h1">Navbar</span>
      </nav>
      </header>
      <Form />
        
    </div>
  );
}

export default App;
