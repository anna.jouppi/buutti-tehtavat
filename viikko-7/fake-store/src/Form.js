import React, {useState, useEffect } from 'react'
import axios from 'axios'


export default function Form() {

    // hooks
    const [productData, setProductData] = useState([])
    const [buttonActivate, setButtonActivate] = useState("")
    const [oneProduct, setOneProduct] = useState([])
    const [dropdown, setDropdown] = useState([])

    //form input states
    const [title, setTitle] = useState("")
    const [category, setCategory] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState("")
    const [image, setImageUrl] = useState("")

    
    // get data from server
    useEffect(() => {
        async function getProductData() {
            const res = await axios.get('https://fakestoreapi.com/products')
            console.log(res.data)
            setProductData(res.data) // save products to state
    
            
        };
        getProductData();
    },[category])


    // add new product
    const formHandler = (e) => {
        e.preventDefault();

        const newProduct = {
            id: new Date().getTime(),
            title: title,
            price: price,
            description: description,
            category: category,
            image: image

        }
        setProductData([...productData].concat(newProduct))
    }

    
    // show all products
    const ShowProducts = () => {

        return (
        <div className='container-all ' > 
            {dropdown.map((product) => 
            
        <div className='item-info row ' key={product.id} >
        <div className='col img-all img-thumbnail' ><img src={product.image} alt="product"/></div>
        <div className='col' > <p className='title-text'>{product.title}</p>
        <p> {product.price} € </p>
        <button type='button'  className='btn btn-success' onClick={() => 
            {setOneProduct(product); setButtonActivate('showDetails')}}>Details</button></div>
        
        </div>)}
        </div>

        )
    }

    // show one product
    const ShowDetails = () => {
        
        console.log("Hello")
        return (
            <div className='container' >
           <div className='row'> 
           <div className='col'>
           <img className='img-one' src={oneProduct.image} alt="product" />
            </div> 
           <div className='col item-one'> <h2>{oneProduct.title}</h2><br/>
           <p className='price' > {oneProduct.price} € </p>
           <p>{oneProduct.description}</p>
           <p> Category: {oneProduct.category} </p>
           <button onClick={() => {setOneProduct(oneProduct); updateOne(oneProduct.id)}} className='btn btn-warning' >Update</button> 
           <button onClick={() => {setButtonActivate("delete"); deleteOne(oneProduct.id)}} className='btn btn-dark' >Delete</button>
            </div>
            </div>
           </div>
        )
    }

    // delete one product
    function deleteOne(id) {
        const updatedProducts = [...productData].filter((product) => product.id !== id)
        setProductData(updatedProducts)

    }

    // update a product with validation
    function updateOne(id) {
        const updatedProduct = [...productData].map((product) => {
            if(product.id === id) {

                if(title !== "") product.title = title
                else product.title = oneProduct.title

                if(category !== "") product.category = category
                else product.category = oneProduct.category

                if(description !== "") product.description = description
                else product.description = oneProduct.description

                if(price !== "") product.price = price
                else product.price = oneProduct.price

                if(image !== "") product.image = image
                else product.image = oneProduct.image

            }
            return product
        })
        setProductData(updatedProduct)
        
    }

    // show by category
    function showByCategory(e) {
        const showCategory = [...productData].filter((product) => product.category === e)
        setDropdown(showCategory)
    }

    // form for adding a new product
  return (
    <div>
        <form className='form-group' >
            <h3>Add a new product with this form</h3>
            <br/>
            <label>Title:</label><br/>
            <input onChange={(e) => setTitle(e.target.value)} className='form-control' type="text"></input><br/>
            <br/>
            <select onChange={(e) => {setButtonActivate("heippa"); setCategory(e.target.value); showByCategory(e.target.value) } } >
                <option >Select a category</option>
                <option value="men's clothing" >Men's clothing</option>
                <option value="women's clothing" >Women's clothing</option>
                <option value="jewelery">Jewelery</option>
                <option value="electronics">Electronics</option>
            </select>
            <br/>
            <br/>
            <label>Description</label>
                <textarea onChange={(e) => setDescription(e.target.value)} className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            <br/>
            <label>Price</label>
                <input onChange={(e) => setPrice(e.target.value)} className='form-control' type="text"></input>
            <br/>
                <label>Image URL</label>
            <input onChange={(e) => setImageUrl(e.target.value)} className='form-control' type="text"></input>

            <br/>
            <br/>
            <button  onClick={formHandler} type='submit' className='btn btn-warning' >Add</button>
            <button  onClick={() => {setButtonActivate('showProducts'); showByCategory()}} type='button' className='btn btn-primary' >Show all</button>
        </form>
        {buttonActivate !== 'showProducts' ? null : <ShowProducts /> }
        {buttonActivate === 'showDetails' ? <ShowDetails /> : null  }
        {buttonActivate === 'delete' ? null : null}
    </div>
  )
}
