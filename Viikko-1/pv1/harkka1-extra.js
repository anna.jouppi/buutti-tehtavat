const x = require("readline");

const z = x.createInterface({ // object
    input: process.stdin,
    output: process.stdout
});

z.question("Kuinka mones kävijä tänään? ", (y) => { // object property
    if (y >= 1000 && y % 25 === 0) {
        console.log("Saa ilmapallon!");
    } else if (y % 2000 === 0) {
        console.log("Saa lahjakortin!!!");
    } else if (y % 2000 !== 0 || (y < 1000 && y % 25 !== 0)) {
        console.log("Ei saa mitään. :(");
    }
    z.close();
});