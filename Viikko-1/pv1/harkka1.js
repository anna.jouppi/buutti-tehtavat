// Discounted price

const price = 90;
const discount = 5;

const finalPrice = price - discount;
console.log(finalPrice);

// Travel time

const distance = 300;
const speed = 80;

const travelTime = distance / speed;
console.log(travelTime);

// Seconds in a year

const days = 365;
const hours = days * 24;
const seconds = 60 * 60;

const secInAYear = hours * seconds;
console.log(secInAYear);

// Area of square
// argv[2]: 2 on ensimmäinen sallittu arvo, 3 seuraava jne. 0 = node, 1 = harkka1.js
const a = process.argv[2];

const square = a * a;

console.log(square);



