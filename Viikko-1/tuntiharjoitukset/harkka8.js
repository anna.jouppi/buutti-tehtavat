let n = 4;


for (let i = 0; i < n; i++) {
    let str = "";
    for (let j = 0; j < i; j++) {
        str += "&";
        console.log("&");
    }
    console.log(str);
}


// 2*n+1
for (let row = 0; row < n; row++) {
    const ampersandAmount = 2 * row +1;
    let str = "";

    for (let i = n - row; i >= 0; i--) {
        str += " ";
    }
    for (let i = 0; i < ampersandAmount; i++) {
        str += "&"
    }
}
