// Largest and smallest

const number_1 = process.argv[2];
const number_2 = process.argv[3];
const number_3 = process.argv[4];

// console.log(number_1, number_2, number_3);

const arr = [number_1, number_2, number_3];

const largest = Math.max(...arr); // suurin. Pitää käyttää ES6 destucturing syntaksia (...), muuten tulee NaN 
const smallest = Math.min(...arr); // pienin

if (number_1 === number_2 && number_1 === number_3) { // samanarvoisuuden testaaminen
    console.log("They are equal.");
}

console.log("Largest number is " + largest, ", smallest is " + smallest);