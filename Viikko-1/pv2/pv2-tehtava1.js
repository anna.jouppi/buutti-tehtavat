// Hello World part 2

const input = process.argv[2];
console.log(input);

while(input.substring(0,2) !== "en" && input.substring(0,2) !== "se" && input.substring(0,2) !== "fi" ) {
    console.log("Please insert only en, es or fi.");
    break;
}


function changeLanguage() {
    if(input === "en") {
        console.log("Hello World!");
    }
    else if(input === "se") {
        console.log("Hejsan Värld!");
    }
    else if(input === "fi") {
        console.log("Hei Maailma!");
    }
}
changeLanguage();

