const name1 = process.argv[2];
const name2 = process.argv[3];
const name3 = process.argv[4];

const name1Length = name1.length;
const name2Length = name2.length;
const name3Length = name3.length;

const arr = [name1, name2, name3];

let strLength = 0;
let longest;

// selvitetään pisin stringi
for (let i = 0; i < arr.length; i++) {
    if(arr[i].length > strLength) {
        strLength = arr[i].length;
        longest = arr[i];
    }
}

// lyhin stringi reduce-metodilla


let shortest;
function shortestString() {
    arr.reduce(function(a,b) {
        shortest = a.length <= b.length ? a : b;
        return shortest;
    });
}
shortestString();

console.log(name1Length, name2Length, name3Length);
console.log(`${longest} ${shortest}`);