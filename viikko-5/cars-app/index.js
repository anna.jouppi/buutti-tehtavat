import express from "express";
import dotenv from "dotenv";
import {} from 'dotenv/config';
import mongoose from "mongoose";
import fs from "fs";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

function readJsonFile() {
    try {
        JSON.parse(fs.readFileSync("user_cars.json", "utf-8"));
        console.log("user_cars parsed!")
    } catch(error) {
        console.log("error happened here!");
        console.log(error);
    }
}

readJsonFile();

async function connectMongoose() {
    await mongoose.connect(
        process.env.CONNECTION_URL,
        { 
            useNewUrlParser: true,
            useUnifiedTopology: true,
            user: process.env.USER,
            pass: process.env.PASS
        }

    ).catch(error => handleError(error));
}

const db = mongoose.connection
db.on('error', (error) => console.error(error));
db.once('open', () => console.log("Database connected successfully!"));


connectMongoose();

const usercarSchema = mongoose.Schema({
    owner_firstname: { type: String, required: false },
    owner_surname: { type: String, required: false },
    contact_email: { type: String, required: false } ,
    owner_address: { type: String, required: false },
    owner_state: { type: String, required: false },
    car_make: { type: String, required: false },
    car_model: { type: String, required: false },
    car_modelyear: { type: Number, required: false },
    price: { type: Number, required: false }
})

const Usercar = mongoose.model("Usercars", usercarSchema, "user_cars");

app.get("/userCar", async(req, res) => {
    const usercars = await Usercar.find();
    res.send(usercars);
})

app.get("/userCar/:id", async(req, res) => {
    const onecar = await Usercar.findById(req.params.id);
    res.send(onecar);
})

app.post("/usedCar", async (req, res) => {
    const car = req.body;

    const newcar = new Usercar({
        owner_firstname: car.owner_firstname,
        owner_surname: car.owner_surname,
        contact_email: car.contact_email,
        owner_address: car.owner_address,
        owner_state: car.owner_state,
        car_make: car.car_make,
        car_model: car.car_model,
        car_modelyear: car.car_modelyear,
        price: car.price
    }) 
    newcar.save().then(() => {
        res.status(200).send(`New car added with an id of ${newcar._id}!`);
    }).catch(err => {
        res.status(500).send(err);
    });
})


app.listen(3000, () => console.log("Server started!"))