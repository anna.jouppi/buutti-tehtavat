import express from "express";
import dotenv from "dotenv";
import {} from 'dotenv/config';
import mongoose from "mongoose";
import bankRouter from "./routes/bankroutes.js"


const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/bank', bankRouter);

mongoose.connect(process.env.CONNECTION_URL, 
    { useNewUrlParser: true,
        useUnifiedTopology: true,
        user: process.env.USER,
        pass: process.env.PASS })

const db = mongoose.connection;
db.on('error', (error) => console.error(error))
db.once('open', () => console.log("Database connection successful!"))

app.listen(3000, () => {
    console.log("Listening to port 3000")
})

export default db;