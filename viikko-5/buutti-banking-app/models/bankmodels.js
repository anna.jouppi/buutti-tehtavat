import mongoose from "mongoose";

const userSchema = new mongoose.Schema({

    id: { type: String, required: false},
    name: { type: String, required: false},
    password: { type: String, required: false},
    init_deposit: { type: Number, required: false},
    deposit: { type: Number, required: false},
    recipient_id: { type: String, required: false},
    amount: { type: Number, required: false}

});

const User = mongoose.model("User", userSchema);

export default User