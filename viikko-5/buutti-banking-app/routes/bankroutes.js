import express from "express";
import * as bankController from "../controllers/bankcontroller.js"
const router = express.Router();

router.get('/', bankController.getAllUsers);

// Create a new user
router.post('/user', bankController.postUser);

// Get account balance
router.get('/:user_id/balance', bankController.getBalance);

// Withdraw money
router.patch('/user/withdraw', bankController.withdrawMoney)

// Deposit money
router.patch('/user/deposit', bankController.depositMoney)

// Transfer money
router.post('/transfer', bankController.transferMoney)

// Update username
router.patch('/update', bankController.updateUserName)

// Update password
router.patch('/user/password', bankController.updatePassword)

export default router;