import User from "../models/bankmodels.js";
import short from "short-uuid";


const user_id = short.generate();
console.log(user_id);

// test get all
export const getAllUsers = async (req, res) => {
    const users = await User.find();
    res.json(users);
}

// create a new user. Code 201: successfully created a new object
export const postUser = async (req, res) => {
        const user = req.body;
    try {
        const newUser = new User({
            id: user_id,
            name: user.name,
            password: user.password,
            init_deposit: user.init_deposit
        })
        newUser.save().then(() => {
            res.status(201).send(`Welcome to Buutti Bank! Your user id is ${newUser.id}`)
        })

    } catch (err) {
        res.status(400).send(err);
    }
}

// get account balance
export const getBalance = async (req, res) => {
    try {
        const user = await User.findById(req.params.user_id);
        res.send(`Your current balance is ${JSON.stringify(user.init_deposit)} €.`);
    } catch(err) {
        res.status(500).send(err);
    }
}

// withdraw money
export const withdrawMoney = async (req, res) => {
    try {
    const user = await User.findByIdAndUpdate(req.body.id, {init_deposit: req.body.init_deposit - req.body.amount})
    const new_account_balance = user.init_deposit;
     res.status(201).send(`Your new account balance is ${JSON.stringify(new_account_balance)}`);
    } catch(err) {
        res.status(500).send(err);
    }
}

// deposit money
export const depositMoney = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.body.id, {init_deposit: req.body.init_deposit + req.body.amount})
        const new_account_balance = user.init_deposit;
         res.status(201).send(`Your new account balance is ${JSON.stringify(new_account_balance)}`);
        } catch(err) {
            res.status(500).send(err);
        }
    }

// transfer money
export const transferMoney = async (req, res) => {
    const user = req.body;
    try {
        const newTransfer = new User({
            recipient_id: user.recipient_id,
            id: user.id,
            amount: user.amount,

            
        })
        const new_account_balance = user.init_deposit - user.amount
        newTransfer.save().then(() => {
            res.status(201).send(`Your transfer is complete. New account balance: ${new_account_balance}`)
        })

    } catch (err) {
        res.status(400).send(err);
    }

}

export const updateUserName = async (req, res) => {
    const user = req.body;
    try {
        await User.findByIdAndUpdate(user.id,{ name: user.name })
        const new_username = user.name;
        res.status(201).send(`Your new username is: ${new_username}`)
    } catch(err) {
        res.status(400).send(err)
    }
}

export const updatePassword = async (req, res) => {
    const user = req.body;
    try {
        await User.findByIdAndUpdate(user.id,{ password: user.password })
        const new_password = user.password;
        res.status(201).send(`Your new password is: ${new_password}`);
    } catch(err) {
        res.status(400).send(err)
    }
}


