import React from 'react'
import ToDo from './ToDo'

export default function TodoList({todoObject, toggleTodo}) {
  return (
    console.log(todoObject),
    todoObject.map(todo => {
        return <ToDo key={todo.id} toggleTodo={toggleTodo} todoObject={todo}/>
        
        
    })
  )
}
