import React from 'react'

export default function ToDo({todoObject, toggleTodo}) {
    function handleTodoClick() {
        toggleTodo(todoObject.id)
    }
  return (
    <div>
        <label>
            <input type="checkbox" checked={todoObject.completed} onChange={handleTodoClick}/>
        {todoObject.name}
        </label>
        </div>
  )
}
