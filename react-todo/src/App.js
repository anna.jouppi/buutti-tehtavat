import './App.css';
import React, {useState, useRef, useEffect} from 'react';
import TodoList from './TodoList';
import {v4 as uuidv4} from 'uuid'

const LOCAL_STORAGE_KEY = 'todoApp.todos'

function App() {
  const [todos, setTodos] = useState([])
  const todoNameRef = useRef();

 // load array from local storage
  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
    if (storedTodos) setTodos(storedTodos)
  },[])

  // save array to local storage
  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos))
  }, [todos])

  // toggle checked todos. Always create a copy first of the state and then create a new state
  function toggleTodo(id) {
    const newTodos = [...todos]
    const todo = newTodos.find(todo => todo.id === id)
    todo.completed = !todo.completed
    setTodos(newTodos)
  }

  // add new todo
  function handleAddTodo(e) {

    const name = todoNameRef.current.value;
    
    if(name === '') return
    console.log(name)
    setTodos(prevTodos => {
      return [...prevTodos, {id: uuidv4(), name: name, completed: true}]
    })
    todoNameRef.current.value = null // clears input field
  }

  // clear completed todos
  function handleClearTodos() {
    const newTodos = todos.filter(todo => !todo.completed)
    setTodos(newTodos);
  }

  return (
    <div className="App">
      <header>
      <h1>Anna's Todo List</h1>
      <img src='girls.jpg' alt='Onni'/>
      </header>
      <input ref={todoNameRef} type="text" /><br/>
      <button onClick={handleAddTodo}>Add a new task</button><br/>
      <button onClick={handleClearTodos}>Clear Completed Tasks</button>
      <br/>
      <br/>
      <TodoList todoObject={todos} toggleTodo={toggleTodo}/>
      <div>{todos.filter(todo => !todo.completed).length}</div>
    </div>
  );
}

export default App;
